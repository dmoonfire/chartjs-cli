#!/usr/bin/env nodejs

// Import required for the project.
const ChartNode = require('chartjs-node');
const Promise = require('bluebird');

const jsonfile = Promise.promisifyAll(require('jsonfile'));
const yargs = require('yargs');

// Create the arguments for this CLI and parse them.
var args = yargs
    .usage('$0 [options] chart.json chart.png')
    .alias({
        'w': 'width',
        'h': 'height',
        'f': 'format',
    })
    .default({
        'w': 600,
        'h': 600,
        'f': 'png'
    })
    .demand(2)
    .help('help')
    .argv;

// Create the basic chart container.
var chart = new ChartNode(args.width, args.height);

// Read in the chart file which contains all the data. This is the
// `chartJsOptions` that is used in most Chart.js examples. Once we
// have that, we write out the image.
var jsonFileName = args._[0];
var imageFileName = args._[1];

jsonfile
    .readFileAsync(jsonFileName)
    .then(options => chart.drawChart(options))
    .then(stream => {
        return chart.writeImageToFile(`image/${args.f}`, imageFileName);
    });
