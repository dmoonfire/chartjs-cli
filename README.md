chartjs-cli
===========

> A command-line tool for creating charts using Chart.js.

## Setup and usage

Install `chartjs-cli` using `npm`:

```sh
npm install --global chartjs-cli
```

This will install the `chartjs` tool which is used to create images from Chart.js source files. The basic usage is:

```sh
chartjs chart.json chart.png
```

The `chart.json` is the Chart.js options.

```json
{
    "type": "bar",
    "data": {
        "labels": ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        "datasets": [{
            "label": "# of Votes",
            "data": [12, 19, 3, 5, 2, 3],
            "backgroundColor": [
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)"
            ],
            "borderColor": [
                "rgba(255,99,132,1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)"
            ],
            "borderWidth": 1
        }]
    },
    "options": {
        "scales": {
            "yAxes": [{
                "ticks": {
                    "beginAtZero":true
                }
            }]
        }
    }
}
```

## Options

There are a number of options to control the output.

### format, f

*string*

Default: `png`

The format of the image: `png`, `jpeg`, `gif`.

### height, h

*number*

Default: `600`

The height of the resulting image in pixel.

### width, w

*number*

Default: `600`

The width of the resulting image in pixel.
